import React, { useContext, useState } from "react";
import { Stage, Layer, Line, Rect, Circle, Text } from "react-konva";
import propTypes from "prop-types";
import AuthContext from "../../../Providers/Context/AuthContext";
import ExceptionContext from "../../../Providers/Context/ExceptionContext";
import { BASE_URL, exceptionStatus, isErrorResponse } from "../../../Constants/Constant";
import DrawTextWidget from '../../Base/DrawingWidgets/DrawTextWidget/DrawTextWidget';
import DrawRectangleWidget from '../../Base/DrawingWidgets/DrawRectangleWidget/DrawRectangleWidget';
import DrawCircleWidget from '../../Base/DrawingWidgets/DrawCircleWidget/DrawCircleWidget';
import DrawBrushWidget from '../../Base/DrawingWidgets/DrawBrushWidget/DrawBrushWidget';
import DrawPencilWidget from '../../Base/DrawingWidgets/DrawPencilWidget/DrawPencilWidget';
import TextBoxKonva from '../../Base/DrawingWidgets/DrawTextWidget/TextBox';
import { withRouter } from 'react-router-dom';
import DrawEraserWidget from '../../Base/DrawingWidgets/DrawEraserWidget/DrawEraserWidget';
import CommentInput from '../../Base/Comments/CommentInput';
import SingleComment from '../../Base/Comments/SingleComment';

const DrawingPage = ({
  color,
  match,
  shareMouse,
  sharedUsers,
  shareMouseHandler,
  location,
  isShareMouse,
  isDrawShape,
  shapes,
  setShapes,
  setComments,
  comments,
  isAddComment,
  setIsAddComment,
  stageRef,
}) => {
  const { user } = useContext(AuthContext);
  const { setOpen } = useContext(ExceptionContext);
  const [isErase, setIsErase] = useState(false);
  const [strokeWidth, setStrokeWidth] = useState(2);

  const [shape, setShape] = useState({
    lines: {
      points: [],
      type: "lines",
      startDrawing: false,
      isDrawing: false,
      stroke: "black",
      strokeWidth : strokeWidth,
      drawingFunc: (obj) => <Line {...obj} />,
      updateSize: (e, x, y, prev) => {
        if (prev.points.length !== 0) {
          if (prev.points.toString().length > 4000) {
            setOpen({
              value: true,
              msg: "Maximum line complexity reached. Please use another tool.",
              statusType: exceptionStatus.warning,
            });
            mouseUp();
          } else {
            if (
              Math.abs(prev.points[prev.points.length - 1] - y) > 5 ||
              Math.abs(prev.points[prev.points.length - 2] - x) > 5
            ) {
              setShape( pre => ({
                ...pre,
                lines: { ...pre.lines, points: [...pre.lines.points, x, y] },
              }));
            }
          }
        } else {
          setShape(pre => ({
            ...pre,
            lines: { ...pre.lines, points: [...pre.lines.points, x, y] },
          }));
        }
      },
      endDrawing: (prop) => {
        const sendObj = {
          points: prop.points.join(','),
          stroke: prop.stroke,
          strokeWidth: prop.strokeWidth,
        }
        fetch(`${BASE_URL}/whiteboards/${match.params.id}/lines`, {
          method: 'POST',
          headers: {
            Authorization: localStorage.getItem("token"),
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(sendObj)
        })
        .then( r => r.json())
        .then( resp => {
          isErrorResponse(resp);
          setShapes((prev) => [...prev, resp]);
          setShape((prev) => ({
          ...shape,
          lines: {
            ...prev.lines,
            startDrawing: false,
            points: [],
          },
        }));
        isDrawShape(resp);
        })
        .catch( err => setOpen({
          value: true,
          msg: err.message,
          statusType: exceptionStatus.error,
        }))
      },
      onStartDrawing: (shapeType, color, x, y, strokeWidth) =>
        setShape((prev) => ({
          ...prev,
          [shapeType]: {
            ...prev[shapeType],
            startDrawing: true,
            stroke: color,
            strokeWidth: strokeWidth,
          },
        })),
        clearDrawingObj: () => setShape((prev) => ({
          ...shape,
          lines: {
            ...prev.lines,
            startDrawing: false,
            points: [],
          },
        }))
    },
    circles: {
      type: "circles",
      startDrawing: false,
      isDrawing: false,
      stroke: "black",
      fill: "",
      strokeWidth: strokeWidth,
      radius: 0,
      drawingFunc: (obj) => <Circle {...obj} />,
      updateSize: (e, x, y, prev) => {
        const differenceX = Math.abs(prev.x - x);
        const differenceY = Math.abs(prev.y - y);
        const newRadius = differenceX > differenceY ? differenceX : differenceY;
        setShape({
          ...shape,
          circles: { ...prev, radius: newRadius },
        });
      },
      endDrawing: (prop) => {
        const sendObj = {
          x: prop.x,
          y: prop.y,
          fill: prop.fill,
          radius: prop.radius,
          stroke: prop.stroke,
          strokeWidth: prop.strokeWidth,
        }
        fetch(`${BASE_URL}/whiteboards/${match.params.id}/circles`, {
          method: 'POST',
          headers: {
            Authorization: localStorage.getItem("token"),
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(sendObj)
        })
        .then( r => r.json())
        .then( resp => {
          isErrorResponse(resp);
          setShapes((prev) => [...prev, resp]);
          setShape((prev) => ({
          ...shape,
          circles: {
            ...prev.circles,
            startDrawing: false,
            x: 0,
            y: 0,
            radius: 0,
          },
        }));
        isDrawShape(resp);
        })
        .catch( err => setOpen({
          value: true,
          msg: err.message,
          statusType: exceptionStatus.error,
        }))

      },
      onStartDrawing: (shapeType, color, x, y, strokeWidth) =>
        setShape((prev) => ({
          ...prev,
          [shapeType]: {
            ...prev[shapeType],
            startDrawing: true,
            stroke: color,
            fill: (prev[shapeType].fill.length !== 0 ? color : ""),
            x,
            y,
            radius: 10,
            strokeWidth,
          },
        })),
        clearDrawingObj: () => setShape((prev) => ({
          ...shape,
          circles: {
            ...prev.circles,
            startDrawing: false,
            x: 0,
            y: 0,
            radius: 0,
          },
        }))
    },
    rectangles: {
      type: "rectangles",
      startDrawing: false,
      isDrawing: false,
      stroke: "black",
      strokeWidth: 1,
      fill: "",
      height: 0,
      width: 0,
      drawingFunc: (obj) => <Rect {...obj} />,
      updateSize: (e, x, y, prev) => {
        setShape({
          ...shape,
          rectangles: { ...prev, width: x - prev.x, height: y - prev.y },
        });
      },
      endDrawing: (prop) => {
        const sendObj = {
          x: prop.x,
          y: prop.y,
          fill: prop.fill,
          height: prop.height,
          width: prop.width,
          stroke: prop.stroke,
          strokeWidth: prop.strokeWidth,
        }
        fetch(`${BASE_URL}/whiteboards/${match.params.id}/rectangles`, {
          method: 'POST',
          headers: {
            Authorization: localStorage.getItem("token"),
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(sendObj)
        })
        .then( r => r.json())
        .then( resp => {
          isErrorResponse(resp);
          setShapes((prev) => [...prev, resp]);
          setShape((prev) => ({
          ...shape,
          rectangles: {
            ...prev.rectangles,
            startDrawing: false,
            x: 0,
            y: 0,
            height: 0,
            width: 0,
          },
        }));
        isDrawShape(resp);
        })
        .catch( err => setOpen({
          value: true,
          msg: err.message,
          statusType: exceptionStatus.error,
        }))

      },
      onStartDrawing: (shapeType, color, x, y, strokeWidth) =>
        setShape((prev) => ({
          ...prev,
          [shapeType]: {
            ...prev[shapeType],
            startDrawing: true,
            stroke: color,
            fill: (prev[shapeType].fill.length !== 0 ? color : ""),
            x,
            y,
            height: 10,
            width: 10,
            strokeWidth,
          },
        })),
        clearDrawingObj: () => setShape((prev) => ({
          ...shape,
          rectangles: {
            ...prev.rectangles,
            startDrawing: false,
            x: 0,
            y: 0,
            height: 0,
            width: 0,
          },
        }))
    },
    textBoxes: {
      type: "textBoxes",
      startDrawing: false,
      isDrawing: false,
      text: "",
      fontSize: 12,
      fontStyle: "normal",
      fill: "black",
      textDecoration: '',
      drawingFunc: (obj) => <Text {...obj} />,
      updateSize: (prop, value) => {
        setShape(prev => ({
          ...shape,
          textBoxes: { ...prev.textBoxes, [prop]: value },
        }));
      },
      endDrawing: (prop) => {
        const sendObj = {
          x: prop.x,
          y: prop.y,
          fill: prop.fill,
          text: prop.text,
          fontStyle: prop.fontStyle,
          fontSize: prop.fontSize,
          textDecoration: prop.textDecoration,
        }
        fetch(`${BASE_URL}/whiteboards/${match.params.id}/textBoxes`, {
          method: 'POST',
          headers: {
            Authorization: localStorage.getItem("token"),
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(sendObj)
        })
        .then( r => r.json())
        .then( resp => {
          isErrorResponse(resp);
          setShapes((prev) => [...prev, resp]);
          setShape((prev) => ({
          ...shape,
          textBoxes: {
            ...prev.textBoxes,
            startDrawing: false,
            text: "",
          },
        }));
        isDrawShape(resp);
        })
        .catch( err => setOpen({
          value: true,
          msg: err.message,
          statusType: exceptionStatus.error,
        }))

      },
      onStartDrawing: (shapeType, color, x, y) => {
        setShape((prev) => ({
          ...prev,
          [shapeType]: {
            ...prev[shapeType],
            startDrawing: true,
            text: "",
            fill: color,
            x,
            y,
          },
        }))},
      clearDrawingObj: () => setShape((prev) => ({
        ...shape,
        textBoxes: {
          ...prev.textBoxes,
          startDrawing: false,
          text: "",
        },
      }))
    },
  });

  const updateShapeProp = (shapeType, prop, token = false, erase = true) => {
    if (token) {
      Object.keys(shape)
      .filter((y) => y !== shapeType)
      .map((x) => (shape[x].isDrawing = false));
    }

    if (erase) {
      setIsErase(false)
    }

    setShape(prev => ({
      ...prev,
      [shapeType]: {
        ...prev[shapeType],
        ...prop,
      }
    }));

  }

  const mouseDown = (e, x, y) => {
    if (!location.pathname.includes('guest')) {
      if (isAddComment.isActive) {
        Object.keys(shape).map((x) => (shape[x].isDrawing = false))
        setIsAddComment(prev => ({...prev, isWrite: true, x, y}))
      }
    }

    const shapeType = Object.keys(shape).find((x) => shape[x].isDrawing);
    if (shapeType) {
      if (shapeType === 'lines' && isErase) {
        shape[shapeType].onStartDrawing(shapeType, '#fafafa', x, y, strokeWidth);
      } else {
        shape[shapeType].onStartDrawing(shapeType, color, x, y, strokeWidth);
      }
    }
  };

  const mouseMove = (e, x, y) => {
    if (isShareMouse) {
      if (
        Math.abs(shareMouse.mouseX - y) > 10 ||
        Math.abs(shareMouse.mouseY - x) > 10
      ) {
        shareMouseHandler(x, y);
      }
    }
    const shapeType = Object.keys(shape).find(
      (x) => shape[x].isDrawing && shape[x].startDrawing
    );
    if (shapeType) {
      if (shapeType !== 'textBoxes') {
        shape[shapeType].updateSize(e, x, y, shape[shapeType]);
      }
    }
  };
  const mouseUp = (e) => {
    const shapeType = Object.keys(shape).find(
      (x) => shape[x].isDrawing && shape[x].startDrawing
    );
    if (shapeType) {
      if (shapeType !== 'textBoxes') {
        if (user) {
          shape[shapeType].endDrawing(shape[shapeType]);
        } else {
          setShapes([...shapes, shape[shapeType]]);
          shape[shapeType].clearDrawingObj();
        }
      }
    }
  };

  const renderSingleDrawingElement = () => {
    const shapeType = Object.keys(shape).find(
      (x) => shape[x].isDrawing && shape[x].isDrawing
    );

    return shape[shapeType]
      ? shape[shapeType].drawingFunc(shape[shapeType])
      : null;
  };

  return (
    <React.Fragment>
      <Stage
        ref={stageRef}
        onMouseDown={(e) => mouseDown(e, e.evt.offsetX, e.evt.offsetY)}
        onMouseMove={(e) => mouseMove(e, e.evt.offsetX, e.evt.offsetY)}
        onMouseUp={(e) => mouseUp(e)}
        height={window.innerHeight}
        width={window.innerWidth}
      >
        <Layer>
          {shapes.length !== 0
            ? shapes.sort((a, b) => a.itemPosition - b.itemPosition).map((x) => shape[x.type].drawingFunc(x))
            : null}
          {renderSingleDrawingElement()}
        </Layer>
      </Stage>
      <TextBoxKonva shapeTextBoxes={shape.textBoxes} setShapes={setShapes} color={color} />
      <div style={{position: 'fixed', bottom: '720px', left: '10px'}}>
      <DrawTextWidget updateShapeProp={updateShapeProp} color={color} />
      <DrawRectangleWidget updateShapeProp={updateShapeProp} color={color} />
      <DrawCircleWidget updateShapeProp={updateShapeProp} color={color} />
      <DrawBrushWidget updateShapeProp={updateShapeProp} color={color} />
      <DrawPencilWidget setStrokeWidth={setStrokeWidth} updateShapeProp={updateShapeProp} color={color} strokeWidth={strokeWidth} />
      <DrawEraserWidget setIsErase={setIsErase} updateShapeProp={updateShapeProp} strokeWidth={strokeWidth} />
      </div>
      {/* {!location.pathname.includes('guest') && sharedUsers.length !== 0
        ? sharedUsers.map((user) => (
            <Avatar
              key={user.id}
              src={`${BASE_URL}/${user.avatar}`}
              alt={`${user.userName}`}
              style={{
                position: "absolute",
                top: user.mouseX,
                left: user.mouseY,
              }}
            />
          ))
        : null} */}
        { !location.pathname.includes('guest') ? <CommentInput isAddComment={isAddComment} setComments={setComments} setIsAddComment={setIsAddComment} isDrawShape={isDrawShape} /> : null }
      { comments.length && !location.pathname.includes('guest') ? comments.map( x => <SingleComment key={x.id} {...x} />) : null }
    </React.Fragment>
  );
};

DrawingPage.propTypes = {
  color: propTypes.string.isRequired,
  currentWhiteboard: propTypes.object.isRequired,
  match: propTypes.object.isRequired,
  shareMouse: propTypes.object.isRequired,
  setShareMouse: propTypes.func.isRequired,
  sharedUsers: propTypes.array.isRequired,
  shareMouseHandler: propTypes.func.isRequired,
  location: propTypes.object.isRequired,
  undo: propTypes.func.isRequired,
  history: propTypes.object.isRequired,
  redo: propTypes.func.isRequired,
  isShareMouse: propTypes.bool.isRequired,

};

export default withRouter(DrawingPage);
