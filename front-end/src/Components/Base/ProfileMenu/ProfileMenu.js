import React from "react";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import propTypes from "prop-types";
import { Divider } from '@material-ui/core';
import { withRouter } from 'react-router-dom';

const ProfileMenu = ({
  anchorEl,
  handleClose,
  setIsCreateWhiteboard,
  setIsChangePassword,
  setIsChangeAvatar,
  setIsShareMouse,
  location,
  setIsAddComment,
  downloadURI,
  isShareMouse,
  stopShare,
}) => {

  return (
    <Menu
      style={{ top: "-4px" }}
      id="simple-menu"
      anchorEl={anchorEl}
      keepMounted
      open={Boolean(anchorEl)}
      onClose={handleClose}
    >
      <MenuItem
        onClick={(e) => {
          setIsCreateWhiteboard(true);
          handleClose();
        }}
        style={{ margin: "10px" }}
      >
        Create board
      </MenuItem>
      <MenuItem
        onClick={(e) => {
          downloadURI()
          handleClose();
        }}
        style={{ margin: "10px" }}
      >
        Download board
      </MenuItem>
      <Divider orientation="horizontal" variant='middle' />
      {/* {toggleChangePassword} */}
      <MenuItem
      onClick={(e) => {
        setIsChangePassword(true);
        handleClose();
      }}
      style={{ margin: "10px" }}
    >
      Change password
    </MenuItem>
      <MenuItem
      onClick={(e) => {
        setIsChangeAvatar(true);
        handleClose();
      }}
      style={{ margin: "10px" }}
    >
      Update avatar
    </MenuItem>
      <Divider orientation="horizontal" variant='middle' />
      { !location.pathname.includes('my') ? <MenuItem
      onClick={(e) => {
        if (isShareMouse) {
          stopShare()
        }
        setIsShareMouse(prev => !prev);
        handleClose();
      }}
      style={{ margin: "10px" }}
    >
      { !isShareMouse ? 'Share mouse' : 'Stop sharing' }
    </MenuItem> : null }
    <Divider orientation="horizontal" variant='middle' />
      <MenuItem
      onClick={(e) => {
        setIsAddComment(prev => ({...prev, isActive: true}));
        handleClose();
      }}
      style={{ margin: "10px" }}
    >
      Add comment
    </MenuItem>
    </Menu>


  );
};

ProfileMenu.propTypes = {
  anchorEl: propTypes.oneOfType([propTypes.object, propTypes.bool]).isRequired,
  handleClose: propTypes.func.isRequired,
  setIsChangePassword: propTypes.func.isRequired,
  setIsCreateWhiteboard: propTypes.func.isRequired,
  setIsChangeAvatar: propTypes.func.isRequired,
  setIsShareMouse: propTypes.func.isRequired,
  location: propTypes.object.isRequired,
  setIsAddComment: propTypes.func.isRequired,
  isShareMouse: propTypes.bool.isRequired,
  downloadURI: propTypes.func.isRequired,
  stopShare: propTypes.func.isRequired,
};

export default withRouter(ProfileMenu);
