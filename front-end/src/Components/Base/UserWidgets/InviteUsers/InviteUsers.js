import React, { useContext, useEffect, useState } from "react";
import {
  Modal,
  makeStyles,
  Fade,
  Container,
  Grid,
  Button,
  TextField,
  Typography,
  CircularProgress,
} from "@material-ui/core";
import Backdrop from "@material-ui/core/Backdrop";
import {
  BASE_URL,
  exceptionStatus,
  isErrorResponse,
} from "../../../../Constants/Constant";
import propType from 'prop-types';
import LoadingContext from "../../../../Providers/Context/LoadingContext";
import ExceptionContext from "../../../../Providers/Context/ExceptionContext";
import Loading from "../../../Page/Loading/Loading";
import { withRouter } from "react-router-dom";
import AuthContext from "../../../../Providers/Context/AuthContext";
import Autocomplete from "@material-ui/lab/Autocomplete";

const InviteUsers = ({
  isInviteUser,
  setIsInviteUser,
  inviteUserHandler,
  history,
}) => {
  const useStyles = makeStyles((theme) => ({
    modal: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
    },
    paper: {
      backgroundColor: theme.palette.background.paper,
      border: "2px solid #000",
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
    },
    form: {
      width: "100%",
      marginTop: theme.spacing(3),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));
  const classes = useStyles();

  const { user } = useContext(AuthContext);
  const { setOpen } = useContext(ExceptionContext);
  const [openAutocomplete, setOpenAutocomplete] = useState(false);
  const [options, setOptions] = useState([]);
  const loading = openAutocomplete && options.length === 0;


  useEffect(() => {

    if (!loading) {
      return undefined;
    }

    fetch(`${BASE_URL}/users`, {
      headers: {
        Authorization: localStorage.getItem("token"),
      },
    })
        .then((r) => r.status >= 500 ? history.push('/servererror') : r.json())
      .then((resp) => {
        isErrorResponse(resp);
        setOptions(resp.filter( x => x.id !== user.id));
      })
      .catch((err) =>
        setOpen({
          value: true,
          msg: err.message,
          statusType: exceptionStatus.error,
        })
      );
  }, [loading, setOpen, user]);

  const autocomplete = (
    <Autocomplete
    onKeyPress={(e) => {
      if (e.key === "Enter") {
        e.preventDefault()
      }
    }}
      id="asynchronous-demo"
      open={openAutocomplete}
      onOpen={() => {
        setOpenAutocomplete(true);
      }}
      onClose={() => {
        setOpenAutocomplete(false);
      }}
      getOptionSelected={(option, value) => option.userName === value.userName}
      getOptionLabel={(option) => option.userName}
      options={options}
      loading={loading}
      renderInput={(params) => (
        <TextField
          {...params}
          onKeyPress={(e) => {
            if (e.key === "Enter") {
              const selectUser = options.find((x) => x.userName === e.target.value);
              if (selectUser) {
                inviteUserHandler(selectUser.userName)
                setOpen({
                  value: true,
                  msg: `User: ${selectUser.userName} invited`,
                  statusType: exceptionStatus.info,
                })
              } else {
                setOpen({
                  value: true,
                  msg: `User: ${e.target.value} not exist`,
                  statusType: exceptionStatus.error,
                })
              }
            }
          }}
          onKeyDown={(e) => {
            if (e.key === "Escape") {
              setIsInviteUser(false);
            }
          }}
          label="Search user"
          variant="outlined"
          InputProps={{
            ...params.InputProps,
            endAdornment: (
              <React.Fragment>
                {loading ? (
                  <CircularProgress color="inherit" size={20} />
                ) : null}
                {params.InputProps.endAdornment}
              </React.Fragment>
            ),
          }}
        />
      )}
    />
  );

  return (
    <Modal
      aria-labelledby="transition-modal-title"
      aria-describedby="transition-modal-description"
      className={classes.modal}
      open={isInviteUser}
      onClose={(e) => setIsInviteUser(false)}
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500,
      }}
    >
      <Fade in={isInviteUser}>
        <div className={classes.paper}>
          {loading ? <Loading /> : null}
          <Container component="main" maxWidth="xl">
            <Typography component="h1" variant="h5" align="center">
              Invite User
            </Typography>
            <form className={classes.form} noValidate>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  {autocomplete}
                </Grid>
                
                <Grid item xs={12}>
                  {/* <Button
                    type="button"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                    onClick={(e) => {
                      e.preventDefault();
                      inviteUserHandler();
                    }}
                  >
                    Invite
                  </Button> */}
                  <Button
                    type="button"
                    fullWidth
                    variant="contained"
                    color="primary"
                    style={{backgroundColor: "#6fa241"}}
                    onClick={(e) => {
                      setIsInviteUser(false);
                    }}
                  >
                    Cancel
                  </Button>
                </Grid>
              </Grid>
            </form>
          </Container>
        </div>
      </Fade>
    </Modal>
  );
};

InviteUsers.propType = {
  history: propType.object.isRequired,
  isInviteUser: propType.bool.isRequired,
  setIsInviteUser: propType.func.isRequired,
  inviteUserHandler: propType.func.isRequired
};
export default withRouter(InviteUsers);
