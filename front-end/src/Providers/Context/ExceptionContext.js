import { createContext } from 'react';

const ExceptionContext = createContext({
    open: {
        value: false,
        msg: '',
        statusCode: null,
        statusType: ''
    },
    setOpen: () => {}
});

export default ExceptionContext
